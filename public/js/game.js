$(document).ready(function () {

    var player = window.player;

    $('.menu div').click(function () {

        $('#pokeball-tab').hide();
        $('#bag-tab').hide();
        $('#chat-tab').hide();

        var id = $(this).attr('id');
        $('#' + id + '-tab').show();
    });

    $('.give-up').click(function () {
        $('.fight-area').remove();
        alertWarning('You lose!','alert-danger');
    });
    // бой с тренером
    $('body').on('click', '.fight-button', function () {
        var id_player = $(this).attr('data-id');
        $.ajax({
            type: "GET",
            url: "/api/opponent/" + id_player,
            success: function (data) {
                if (!$('.fight-area').length) {
                    wildFightStart();
                    setPlayerImg();
                    setPokemonOpponentImg(data['pokemon'][0]);
                    setOpponentImg(data['user'][0]['img']);
                } else {
                    alertWarning("You can't start new fight while you in fight!", 'alert-warning');
                }
            }
        });
    });

    //нападение дикого покемона
    $('.wild-pokemons label [type=checkbox]').click(function () {
        var label = $(this).parent('label');

        if (label.hasClass('btn-success')) {
            var refreshIntervalId = setInterval(wildPokemonsAttack, 5000);
            label.removeClass('btn-success').addClass('btn-danger');
        } else {
            clearInterval(refreshIntervalId);
            label.removeClass('btn-danger').addClass('btn-success');
        }
    });

    //вызов покемона на поле
    $('body').on( 'click','#pokeball-tab .pokemon', function () {

        var id_pokemon = $(this).attr('data-id');

        $.ajax({
            type: "GET",
            url: "/api/pokemon_current/" + id_pokemon,
            success: function (data) {
                console.log(data);
                setPokemonInFight(data);
                getAttacks(data['id']);
            }
        });
    });

    //лечение покемонов
    $('.heal-btn').click(function () {
        $.ajax({
            type: "GET",
            url: "/api/pokemon_current/heal/" + player['id'],
            success: function (data) {
                if (Object.keys(data).length) {
                    alertWarning("All pokémons are cured and their powers restored.", 'alert-success');
                    getMessage(data);
                } else {
                    alertWarning("All pokémons is fine.", 'alert-warning');
                }
            }
        });
    });

    //лечение покемонов
    $('.heal-all-btn').click(function () {
        $.ajax({
            type: "GET",
            url: "/api/heal/all",
            success: function (data) {
                console.log(data);
                $('.pokemon .pokemon-health').css('width', '80%');
            }
        });
    });

    $('#bag').click(function () {
        $.ajax({
            type: "GET",
            url: "/api/thing_current/get_player_things/" + player['id'],
            success: function (data) {
                var things = '';
                data.forEach((thing) => {
                    things += '<div class="thing" data-id="' + thing['id'] + '">' +
                        '<img src="' + '/img/' + thing['img'] + '" >' +
                        '<span class="count">' + thing['count'] + '</span>' +
                        '</div>';
                    $('#bag-tab').html(things);
                });
            }
        });
    });

    $('body').on( 'click','.thing', function () {
        if ($(this).attr('data-id') == 3) {

            var cel = $(this).find('.count');
            var count = parseInt(cel.html());
            if (count != 0) {
                cel.html(count - 1);
                var id_pokemon = $('.pokemon-op-in-fight').attr('data-id');
                $.ajax({
                    type: "POST",
                    url: "/api/pokemon_current/catch/",
                    data: {
                        id_pokemon: id_pokemon,
                        id_player: player['id']
                    },
                    success: function (data) {
                        var pokemon = '<div class="pokemon" data-id="' + data['id'] + '">' +
                            '<img src="'+data['img']+'">' +
                            '<span class="pokemon-health" style="width:' + (data['current_health'] / data['health']) * 0.8 * 100 + '%"></span>' +
                            '<ul class="pokemon-info list-group">' +
                            '<li class="list-group-item">level:' + data['level'] + '</li>' +
                            '<li class="list-group-item">happiness:' + data['happiness'] + '</li>' +
                            '<li class="list-group-item">health:' + data['health'] + '</li>' +
                            '<li class="list-group-item">attack:' + data['attack'] + '</li>' +
                            '<li class="list-group-item">defence:' + data['defence'] + '</li>' +
                            '<li class="list-group-item">speed:' + data['speed'] + '</li>' +
                            '<li class="list-group-item">special_attack:' + data['special_attack'] + '</li>' +
                            '<li class="list-group-item">special_protection:' + data['special_protection'] + '</li>' +
                            '<li class="list-group-item">evolution_points:' + data['evolution_points'] + '</li>' +
                            '</ul>' +
                            '</div>';
                        $('#pokeball-tab').append(pokemon);
                        $('.fight-area').remove();
                        alertWarning('The capture was a success! Now you have new pokemon!','alert-success');
                    }
                });
            }
        }
    });

    function setPokemonInFight(pokemon) {
        var pokemon_el = $('.pokemon-in-fight');
        pokemon_el.attr('data-id', pokemon['id']);
        pokemon_el.css({
            'background': "url('" + pokemon['img'] + "')  no-repeat",
            "background-size": "contain"
        });
        pokemon_el.html(' <span class="pokemon-health" style="width:' + (pokemon['current_health'] / pokemon['health']) * 0.8 * 100 + '%"></span>');
        setPokemonHealth(pokemon_el, pokemon['current_health']);
    }

    function alertWarning(text, status) {
        $('<div class="alert ' + status + '" role="alert">\n' +
            text +
            '</div>').insertAfter('.waiting-block');
        setTimeout(function () {
            $('.alert').remove()
        }, 3000);
    }

    function getMessage(data) {
        var st = 'info-warning';
        var action = '-';
        if (data['status']) {
            st = 'info_success';
            action = '+';
        }
        $('<div class="message-info ' + st + '">' +
            '<img src="/img/' + data['img'] + '" >' +
            '<span class="count">' + action + data['count'] + '</span>' +
            '</div>').insertAfter('h2');
        setTimeout(function () {
            $('.message-info').remove()
        }, 3000);
    }


    //запрос на дикого покемона
    function wildPokemonsAttack() {
        if (!$('.fight-area').length) {
            $.ajax({
                type: "GET",
                url: "/api/fight/wild",
                success: function (data) {
                    wildFightStart();
                    setPlayerImg();
                    setPokemonOpponentImg(data);

                }
            });
        }
    }

    //добавить боевую арену
    function wildFightStart() {
        if (!$('.fight-area').length) {
            $('<div class="fight-area">\n' +
                '<div class="player-in-fight"></div>\n' +
                '<div class="pokemon-in-fight"></div>\n' +
                '<div class="pokemon-op-in-fight"><span class="pokemon-health"></span></div>\n' +
                '<div class="oponent-in-fight"></div>\n' +
                ' </div>').insertAfter('.location .waiting-block');
        }

    }

    //выставить плеера
    function setPlayerImg() {
        var player = window.player;
        $('.player-in-fight').css({
            'background': "url('" + player['img'] + "')  no-repeat",
            "background-size": "contain"
        });
    }

    //выставить опонента
    function setOpponentImg(img) {
        $('.oponent-in-fight').css({
            'background': "url('" + img + "')  no-repeat",
            "background-size": "contain"
        });
    }

    //выставить покемона опонента
    function setPokemonOpponentImg(data) {
        var el = $('.pokemon-op-in-fight');
        el.css({'background': "url('" + data['img'] + "')  no-repeat", "background-size": "contain"});
        el.attr('data-id', data['id'])
        el.find('.pokemon-health').css('width',(data['current_health'] / data['health'] * 0.8 * 100)+'%');
    }

    //получить атаки
    function getAttacks(id_pokemon) {
        $('.attacks').remove();
        $.ajax({
            type: "GET",
            url: "/api/pokemon_current/getattacks/" + id_pokemon,
            success: function (data) {
                var attack_block = '<div class="btn-group btn-group-toggle attacks" data-toggle="buttons">';
                data.forEach((attack) => {
                    var style = 'btn-secondary';
                    var disabled = 'disabled';
                    if (attack['left'] > 0) {
                        style = 'btn-warning';
                        disabled = '';
                    }
                    attack_block += '<label class="btn ' + style + '">\n' +
                        '<input type="radio" name="options" data-attack-left="' + attack['left'] +
                        '" data-attack-id="' + attack['id_attack'] + '" autocomplete="off" ' + disabled + '>' +
                        attack['name'] + '</br><span>\n' +
                        'left:<span class="attack-left">' + attack['left'] + '<span></span>\n' +
                        '</label>'
                });

                attack_block += '</div>';
                $(attack_block).insertAfter('.oponent-in-fight');

            }
        });
    }

    //ожидание
    function moveProgressBar(id_pokemon, el) {
        $('.waiting-block').css('opacity', 1);
        var getPercent = ($('.progress-wrap').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.progress-wrap').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        $('.progress-bar').css('left', progressTotal);
        var animationLength = 2500;

        // on page load, animate percentage bar to data percentage length
        // .stop() used to prevent animation queueing
        $('.progress-bar').stop().animate({
            left: 0
        }, animationLength, function () {
            $('.waiting-block').css('opacity', 0);
            $('.progress-bar').css('left', 0);

        });
    }

    function setPokemonHealth(pokemon, health) {

        var health_line = pokemon.find('.pokemon-health');

        var width = parseInt(health_line.parent().width()) * 0.8;

        var ot = health[0] / health[1];

        if (ot < 0.5) {
            health_line.css('border', '2px solid #e6d321');
        }
        if (ot < 0.2) {
            health_line.css('border', '2px solid rgb(234, 68, 11)');
        }
        var value = width * ot;
        health_line.css('width', value);
    }
});
