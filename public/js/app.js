$(document).ready(function () {

    $('#men').click(function () {
        $('.boys').animate({
            opacity: 1,
            right: 0,
            'z-index': 1,
        }, 500, function () {
        });
        $('.girls').css({opacity: 0, right: 0, left: "unset", 'z-index': 0});

        //hide button and clear choose
        $('input[name="girls"]').prop('checked', false);
        $('input[name="boys"]').prop('checked', false);
        $('.choose-button').css('display', 'none');

    });

    $('#women').click(function () {
        $('.girls').animate({
            opacity: 1,
            left: 0,
            'z-index': 1,
        }, 500, function () {
        });
        $('.boys').css({opacity: 0, left: 0, right: "unset", 'z-index': 0});

        //hide button and clear choose
        $('input[name="girls"]').prop('checked', false);
        $('input[name="boys"]').prop('checked', false);
        $('.choose-button').css('display', 'none');

    });

    $('.girls,.boys').click(function () {
        $('.choose-button').css('display', 'block');
    });

});