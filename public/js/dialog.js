$(document).ready(function () {

    if(window.dialog != null) {
        var dialog = window.dialog;
        dialog = Object.values(dialog);

        for (let i = 0; i < dialog.length; i++) {
            setTimeout(say, i * 2000, dialog[i]['name'], dialog[i]['text']);
        }
        setTimeout(showPokemons, dialog.length * 2000);
    }

    $('.pokemon-choose .pokemon').click(function () {
        var id = $(this).attr('data-id');
        var id_player = $('.player').attr('data-player-id');
        $(this).css('position', 'absolute').animate({
            left: '44%',
            top: '-310%'
        }, 1000, function () {
            $.ajax({
                type: "POST",
                url: "/api/pokemon_current",
                data: {id:id, id_player: id_player },
                success: function(){setTimeout(redirectToMap, 500);}
            });

        });
    });

    function say(who, text) {
        $('.player-dialog').html('');
        var message = '<div class="message-orange">' +
            '<p class="message-content">' + text + '</p>' +
            '</div>';
        $('.' + who).html(message);
    }

    function showPokemons() {
        $('.pokemon-block').show();
    }

    function redirectToMap() {
        window.location.href = "region_kanto";
    }

});
