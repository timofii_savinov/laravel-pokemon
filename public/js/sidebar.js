$(document).ready(function () {
    $('.player-on-location-list li').click(function () {
        var id_player = $(this).attr('data-player-id');

        if (!$('.alert').length) {
            $(this).append('<div class="alert alert-primary action" role="alert">' +

                '<button type="button" class="btn btn-danger fight-button" data-id="'+id_player+'">Fight</button>' +
                '<button type="button" class="btn btn-warning"  exchange-button>Exchange</button></div>');
        }
        //setTimeout(deleteMenu, 1000, $(this));
    });

    function deleteMenu(elem) {
        var menu_action = elem.find('.action');
        menu_action.animate({opacity: "0"}, 700, function () {
            menu_action.remove();

        });
    }
});
