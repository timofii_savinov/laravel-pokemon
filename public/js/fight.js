$(document).ready(function () {


    function moveProgressBar(el) {
        $('.waiting-block').css('opacity',1);
        var getPercent = ($('.progress-wrap').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.progress-wrap').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        $('.progress-bar').css('left',progressTotal);
        var animationLength = 4500;

        // on page load, animate percentage bar to data percentage length
        // .stop() used to prevent animation queueing
        $('.progress-bar').stop().animate({
            left: 0
        }, animationLength, function () {
            $('.waiting-block').css('opacity',0);
            $('.progress-bar').css('left',0);
            attackAftermath(el);
            showAttacks();
        });
    }

    //выбор атаки
    $('body').on('click','.attacks input', function () {
        $('.attacks').remove();
        moveProgressBar($(this));

    });

    function setResult(attack, id_pokemon, id_pok_opponent)
    {
        $.ajax({
            type: "POST",
            url: "/api/fight/wild/",
            data: {id_attack: attack,
                id_pokemon_in_attack: id_pokemon,
                id_pokemon_attaсked: id_pok_opponent},
            success: function (data) {

                setPokemonHealth($('.pokemon[data-id="'+data['pokemon'][2]+'"]'),data['pokemon']);
                setPokemonHealth($('.pokemon-in-fight'),data['pokemon']);
                setPokemonHealth($('.pokemon-op-in-fight'),data['opponent']);

                if(Object.keys(data['winner']).length){
                    console.log('win');
                    $('.fight-area').remove();
                    getMessage(data['winner']);
                    alertWarning('You win!','alert-success');
                }
            }
        });
    }
    function alertWarning(text, status) {
        $('<div class="alert ' + status + '" role="alert">\n' +
            text +
            '</div>').insertAfter('.waiting-block');
        setTimeout(function () {
            $('.alert').remove()
        }, 3000);
    }

    function getMessage(data) {
        var st = 'info-warning';
        var action = '-';
        if (data['status']) {
            st = 'info_success';
            action = '+';
        }
        $('<div class="message-info ' + st + '">' +
            '<img src="/img/' + data['img'] + '" >' +
            '<span class="count">' + action + data['count'] + '</span>' +
            '</div>').insertAfter('h2');

        setTimeout(function () {
            $('.message-info').remove()
        },3000);
    }

    //получить обновленные атаки
    function getAttacks(id_pokemon){

        $.ajax({
            type: "GET",
            url: "/api/pokemon_current/getattacks/" + id_pokemon,
            success: function (data) {
                var attack_block = '<div class="btn-group btn-group-toggle attacks" data-toggle="buttons">';
                data.forEach((attack) => {
                    var style = 'btn-secondary';
                    var disabled = 'disabled';
                    if (attack['left'] > 0) {
                        style = 'btn-warning';
                        disabled = '';
                    }
                    attack_block += '<label class="btn ' + style + '">\n' +
                        '<input type="radio" name="options" data-attack-left="' + attack['left'] +
                        '" data-attack-id="' + attack['id_attack'] + '" autocomplete="off" ' + disabled + '>' +
                        attack['name'] + '</br><span>\n' +
                        'left:<span class="attack-left">' + attack['left'] + '<span></span>\n' +
                        '</label>'
                });

                attack_block += '</div>';
                $(attack_block).insertAfter('.oponent-in-fight');
            }
        });
    }

    //показать атаки
    function showAttacks(){
        var id_pokemon = $('.pokemon-in-fight').attr('data-id');
        getAttacks(id_pokemon);
    }

    //показать результаты боя
    function attackAftermath(elem) {
        var id_pokemon = $('.pokemon-in-fight').attr('data-id');
        var id_pok_opponent = $('.pokemon-op-in-fight').attr('data-id');
        var id_attack = elem.attr('data-attack-id');
        setResult(id_attack,id_pokemon,id_pok_opponent);
    }

    function setPokemonHealth(pokemon, health){


        var health_line = pokemon.find('.pokemon-health');

        var width = parseInt(health_line.parent().width())*0.8;

        var ot = health[0]/health[1];

        if(ot<0.5){
            health_line.css('border', '2px solid #e6d321');
        }
        if(ot<0.2){
            health_line.css('border', '2px solid rgb(234, 68, 11)');
        }
        var value = width*ot;
        health_line.css('width',value);

    }

});
