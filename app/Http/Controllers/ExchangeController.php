<?php

namespace App\Http\Controllers;

use App\Relation_exchange_pp;
use App\Thing_current;
use App\Thing_exchange;
use Illuminate\Http\Request;

/*1. запрос на обмен, добавление в таблицу игрок-игрок
 *2. игрок 1/2 добавляет предмет на обмен
 *3. изменить у предмета владельца
 *4. удалить записи из таблиц
 *
 */

/*механика обмена предметами между игрока*/

class ExchangeController extends Controller
{
    /*
     * даем запрос на обмен
     */
    public function addRequestExchange(Request $request)
    {
        $relation = Relation_exchange_pp::create($request->all());

        return response()->json($relation, 201);
    }

    /*
     * даем согласие на обмен
     */
    public function confirmRequestExchange($id_player_request, $id_player_confirm)
    {
        $relation = Relation_exchange_pp::where('id_player1', $id_player_request)->get();

        $relation->id_paler2 = $id_player_confirm;
        $relation->save();

        return response()->json($relation, 201);
    }

    /*
     * добавляем вещи на обмен
     */
    public function addThingToExchange(Request $request)
    {
        $te = Thing_exchange::create($request->all());

        return response()->json($te, 201);
    }

    /*
     * даем добро на обмен 1й пользователь
     */
    public function exchangeAcceptedPlayer1($id_player)
    {
        $relation = Relation_exchange_pp::where('id_player1', $id_player) > get();
        $relation->id_player1_accepted = 1;
        $relation->save();
        //запуск обмена
        if ($relation->id_player2_accepted)
            $this->changeThingsOwner($relation);
    }

    /*
     * даем добро на обмен 2й пользователь
     */
    public function exchangeAcceptedPlayer2($id_player)
    {
        $relation = Relation_exchange_pp::where('id_player2', $id_player) > get();
        $relation->id_player2_accepted = 1;
        $relation->save();
        //запуск обмена
        if ($relation->id_player1_accepted)
            $this->changeThingsOwner($relation);
    }

    /*
     * меняем у вещей пользователя
     */
    public function changeThingsOwner(Relation_exchange_pp $relation)
    {
        $te1 = Thing_exchange::where('id_owner', $relation->id_player1)->get()->toArray();

        foreach ($te1 as $te => $thing) {
            $tc = Thing_current::find($thing['id_thing_current']);
            //если количество на обмен меньше чем есть
            if ($thing['count'] < $tc->count) {
                //отнимаем у первого
                $tc->count = $tc->count - $thing['count'];
                $tc->save();

                //создаем для второго
                $tc_new = new Thing_current();
                $tc_new->count = $thing['count'];
                $tc_new->id_thing = $tc->id_thing;
                $tc_new->id_owner = $relation->id_player2;
                $tc_new->save();
            } elseif ($thing['count'] == $tc->count) {
                $tc->id_owner = $relation->id_player2;
                $tc->save();
            }

        }

        $te2 = Thing_exchange::where('id_owner', $relation->id_player2)->get()->toArray();

        foreach ($te2 as $te => $thing) {
            $tc = Thing_current::find($thing['id_thing_current']);
            //если количество на обмен меньше чем есть
            if ($thing['count'] < $tc->count) {
                //отнимаем у первого
                $tc->count = $tc->count - $thing['count'];
                $tc->save();

                //создаем для второго
                $tc_new = new Thing_current();
                $tc_new->count = $thing['count'];
                $tc_new->id_thing = $tc->id_thing;
                $tc_new->id_owner = $relation->id_player1;
                $tc_new->save();
            } elseif ($thing['count'] == $tc->count) {
                $tc->id_owner = $relation->id_player1;
                $tc->save();
            }

        }


        if (clearExchange($relation->id_player1, $relation->id_player2)) {
            return response()->json(true, 201);
        }
    }

    /*
     * очищаем записи о связях и обменах в таблицах
     */
    public function clearExchange($id_player1, $id_player2)
    {
        //удалиляем связь игрок-игрок
        $relation = Relation_exchange_pp::where('id_player1', $id_player1)->get();
        $relation->delete();

        //удалиляем предметы на обмен
        $te = Thing_exchange::where('id_owner', [$id_player1, $id_player2])->get();
        $te->delete();

        return true;
    }
}
