<?php

namespace App\Http\Controllers;

use App\Attack;
use App\PokemonAttackLeft;
use App\PokemonCurrent;
use App\Thing;
use App\ThingCurrent;
use App\TypeEffect;
use Illuminate\Http\Request;

/*Контроллер для боев между покемонами*/

class FightController extends Controller
{

    public function stepWild(Request $request)
    {
        $data = [];
        $input = $request->all();
        $id_attack = $input['id_attack'];
        $id_pokemon_in_attack = $input['id_pokemon_in_attack'];
        $id_pokemon_attaсked = $input['id_pokemon_attaсked'];

        $al = new PokemonAttackLeft();
        $al->updateCountLeft($id_attack, $id_pokemon_in_attack);

        $data['opponent'] = $this->getHealthAfterAttack($id_attack, $id_pokemon_in_attack, $id_pokemon_attaсked);
        $data['pokemon'] = $this->getHealthAfterAttack(1, $id_pokemon_attaсked, $id_pokemon_in_attack);

        $data['winner'] = [];
        if($data['opponent'][0] == 1){
            //получить кредиты за победу
            $tc_obj = new ThingCurrent();
            $credits = $tc_obj->addCreditsAfterWin($id_pokemon_in_attack, $id_pokemon_attaсked);
            $data['winner']['status'] = true;
            $data['winner']['count'] = $credits;
            $t_obj = new Thing();
            $data['winner']['img']  = $t_obj->getThingImg(1);
        }


        return response()->json($data, 200);
    }

    private function getHealthAfterAttack($id_attack, $id_pokemon_in_attack, $id_pokemon_attaсked)
    {
        $pok_cur_obj = new PokemonCurrent();
        $pokemon_ina = $pok_cur_obj->getCurrentPokemon($id_pokemon_in_attack);

        $attack_obj = new Attack();
        $attack = $attack_obj->getCurrentAttack($id_attack, $id_pokemon_in_attack);

        //статы от предметов
        $stab = 1;


        $pokemon_a = $pok_cur_obj->getCurrentPokemon($id_pokemon_attaсked);

        //тип эффективности
        $typeef_obj = new TypeEffect();
        $type = $typeef_obj->getTypeEffectValue($pokemon_ina, $pokemon_a);

        //крит
        $crit = $this->getCritical(1);

        $damage = $this->getDamage($pokemon_ina, $pokemon_a, $attack, $stab, $type, $crit);

        //обновим здоровье атакованного покемона
        $health_obj = new PokemonCurrent();
        $health = $health_obj->setPokemonCurrentHealthWithDamage($id_pokemon_attaсked, $damage);

        return [0 => $health, 1 => $pokemon_a->health, 2=>$id_pokemon_attaсked];

    }

    private function getDamage($pokemon, $pokemon_a, $attack, $stab, $type_value, $crit)
    {
        $rn = rand(85, 100);
        /*echo $pokemon->level.','.$pokemon->attack.','.$pokemon->special_attack.','.$pokemon_a->defence.','.$pokemon_a->special_protection.','.
            $attack['power'].','.$type_value.','.$crit.','.$rn."<br>";*/
        $damage = ((2 * $pokemon->level + 10) / 250 * (($pokemon->attack / $pokemon->special_attack) /
                    ($pokemon_a->defence / $pokemon_a->special_protection)) * $attack['power']  +2)
            * $stab * $type_value * $crit * ($rn / 100);
        return round($damage);
    }

    private function getCritical($crit_lvl)
    {
        //сс critical chance
        $cc = 0;

        switch ($crit_lvl) {
            case 1:
                $cc = 7;
                break;
            case 2:
                $cc = 13;
                break;
            case 3:
                $cc = 25;
                break;
            case 4:
                $cc = 33;
                break;
            case 5:
                $cc = 50;
                break;
        }

        $critical_lvl = rand(1, 100);
        if ($critical_lvl <= $cc) {
            return 1.5;
        } else {
            return 1;
        }
    }

}
