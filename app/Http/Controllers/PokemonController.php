<?php

namespace App\Http\Controllers;

use App\Pokemon;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    public function index()
    {
        return Pokemon::all();
    }

    public function show(Pokemon $pokemon)
    {
        return $pokemon;
    }

    public function create(Request $request)
    {
        $pokemon = Pokemon::create($request->all());

        return response()->json($pokemon, 201);
    }

    public function update(Request $request, Pokemon $pokemon)
    {
        $pokemon->update($request->all());

        return response()->json($pokemon, 200);
    }

    public function delete(Pokemon $pokemon)
    {
        $pokemon->delete();

        return response()->json(null, 204);
    }
}
