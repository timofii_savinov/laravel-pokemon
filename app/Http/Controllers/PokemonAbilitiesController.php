<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PokemonAbilitiesController extends Controller
{
    public function index()
    {
        return Abilities::all();
    }

    public function show(Abilities $abilities)
    {
        return $abilities;
    }

    public function create(Request $request)
    {
        $abilities = Abilities::create($request->all());

        return response()->json($abilities, 201);
    }

    public function update(Request $request, Abilities $abilities)
    {
        $abilities->update($request->all());

        return response()->json($abilities, 200);
    }

    public function delete(Abilities $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }
}
