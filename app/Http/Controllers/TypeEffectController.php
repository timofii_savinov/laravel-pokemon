<?php

namespace App\Http\Controllers;

use App\TypeEffect;
use Illuminate\Http\Request;

class TypeEffectController extends Controller
{
    public function index()
    {
        return TypeEffect::all();
    }

    public function show(TypeEffect $effect)
    {
        return $effect;
    }

    public function create(Request $request)
    {
        $effect = TypeEffect::create($request->all());

        return response()->json($effect, 201);
    }

    public function update(Request $request, TypeEffect $effect)
    {
        $effect->update($request->all());

        return response()->json($effect, 200);
    }

    public function delete(TypeEffect $effect)
    {
        $effect->delete();

        return response()->json(null, 204);
    }
}
