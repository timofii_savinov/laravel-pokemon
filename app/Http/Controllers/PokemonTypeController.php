<?php

namespace App\Http\Controllers;

use App\PokemonType;
use Illuminate\Http\Request;

class PokemonTypeController extends Controller
{
    public function index()
    {
        return PokemonType::all();
    }

    public function show(PokemonType $type)
    {
        return $type;
    }

    public function create(Request $request)
    {
        $article = PokemonType::create($request->all());

        return response()->json($article, 201);
    }

    public function update(Request $request, PokemonType $article)
    {
        $article->update($request->all());

        return response()->json($article, 200);
    }

    public function delete(PokemonType $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }
}
