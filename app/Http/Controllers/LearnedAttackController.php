<?php

namespace App\Http\Controllers;

use App\LearnedAttaсk;
use Illuminate\Http\Request;

class LearnedAttackController extends Controller
{
    public function index()
    {
        return LearnedAttaсk::all();
    }

    public function show(Attack $attack)
    {
        return $attack;
    }

    public function create(Request $request)
    {
        $attack = LearnedAttaсk::create($request->all());

        return response()->json($attack, 201);
    }

    public function update(Request $request, Attack $attack)
    {
        $attack->update($request->all());

        return response()->json($attack, 200);
    }

    public function delete(LearnedAttaсk $attack)
    {
        $attack->delete();

        return response()->json(null, 204);
    }
}
