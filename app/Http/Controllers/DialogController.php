<?php

namespace App\Http\Controllers;

use App\Dialog;
use Illuminate\Http\Request;

class DialogController extends Controller
{
    public function index()
    {
        return Dialog::all();
    }

    public function show(Dialog $dialog)
    {
        return $dialog;
    }

    public function create(Request $request)
    {
        $dialog = Dialog::create($request->all());

        return response()->json($dialog, 201);
    }

    public function update(Request $request, Dialog $dialog)
    {
        $dialog->update($request->all());

        return response()->json($dialog, 200);
    }

    public function delete(Dialog $dialog)
    {
        $dialog->delete();

        return response()->json(null, 204);
    }
}
