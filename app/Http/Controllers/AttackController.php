<?php

namespace App\Http\Controllers;

use App\Attack;
use Illuminate\Http\Request;

class AttackController extends Controller
{
    public function index()
    {
        return Attack::all();
    }

    public function show(Attack $attack)
    {
        return $attack;
    }

    public function create(Request $request)
    {
        $attack = Attack::create($request->all());

        return response()->json($attack, 201);
    }

    public function update(Request $request, Attack $attack)
    {
        $attack->update($request->all());

        return response()->json($attack, 200);
    }

    public function delete(Attack $attack)
    {
        $attack->delete();

        return response()->json(null, 204);
    }
}
