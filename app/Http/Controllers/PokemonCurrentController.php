<?php

namespace App\Http\Controllers;

use App\AllowableNumberAttacks;
use App\Attack;
use App\LearnedAttaсk;
use App\Pokemon;
use App\PokemonAttackLeft;
use App\PokemonCurrentHealth;
use App\PokemonCurrent;
use App\PokemonGencode;
use App\Thing;
use App\ThingCurrent;
use Illuminate\Http\Request;
use Auth;

class PokemonCurrentController extends Controller
{
    public function index()
    {
        return PokemonCurrent::all();
    }

    public function show($id)
    {
        $pc_obj = new PokemonCurrent();
        $pokemon = $pc_obj->getCurrentPokemon($id);
        return $pokemon;
    }

    public function create(Request $request)
    {
        $pokemon = Pokemon::where('id', $request->input('id'))->first();

        $user = $request->input('id_player');

        $char_id = rand(1, 26);

        $current = new PokemonCurrent;

        $current->id_pokemon = $pokemon->id;
        $current->id_character = $char_id;
        $current->id_catch_player = $user;
        $current->id_owner_player = $user;

        $current->save();

        //создаем ген код
        $gen_obg = new PokemonGencode();
        $gen_obg->createGen($current->id);

        //обновляем статы
        $pokemon_obj = new Pokemon();
        $current->health = $pokemon_obj->getStatValue($current->id, $pokemon->health, 'health', $char_id);
        $current->current_health = $current->health;
        $current->attack = $pokemon_obj->getStatValue($current->id, $pokemon->attack, 'attack', $char_id);
        $current->defence = $pokemon_obj->getStatValue($current->id, $pokemon->defence, 'defence', $char_id);
        $current->speed = $pokemon_obj->getStatValue($current->id, $pokemon->speed, 'speed', $char_id);
        $current->special_attack = $pokemon_obj->getStatValue($current->id, $pokemon->special_attack, 'special_attack', $char_id);
        $current->special_protection = $pokemon_obj->getStatValue($current->id, $pokemon->special_protection, 'special_protection', $char_id);
        $current->img = $pokemon->img;

        $current->save();

        //добавить атаку
        $la = new LearnedAttaсk;
        $la->id_pokemon_current = $current->id;
        $la->id_attack = 5;
        $la->save();

        //добавить в таблицу количество
        $al = new PokemonAttackLeft;
        $al->id_pokemon_current = $current->id;
        $al->id_attack = 1;
        $al->left = 10;
        $al->save();

        return response()->json($current, 201);
    }

    public function createWildPokemon()
    {
        $pokemon = Pokemon::where('id', 7)->first();
        $current = new PokemonCurrent;

        $char_id = rand(1, 26);
        $current->id_pokemon = $pokemon->id;
        $current->id_character = $char_id;
        $current->id_catch_player = 0;
        $current->id_owner_player = 0;

        $current->save();

        //создаем ген код
        $gen_obg = new PokemonGencode();
        $gen_obg->createGen($current->id);

        //обновляем статы
        $pokemon_obj = new Pokemon();
        $current->health = $pokemon_obj->getStatValue($current->id, $pokemon->health, 'health', $char_id);
        $current->current_health = $current->health;
        $current->attack = $pokemon_obj->getStatValue($current->id, $pokemon->attack, 'attack', $char_id);
        $current->defence = $pokemon_obj->getStatValue($current->id, $pokemon->defence, 'defence', $char_id);
        $current->speed = $pokemon_obj->getStatValue($current->id, $pokemon->speed, 'speed', $char_id);
        $current->special_attack = $pokemon_obj->getStatValue($current->id, $pokemon->special_attack, 'special_attack', $char_id);
        $current->special_protection = $pokemon_obj->getStatValue($current->id, $pokemon->special_protection, 'special_protection', $char_id);
        $current->img = $pokemon->img;
        $current->save();



        return response()->json($current, 201);

    }

    public function catch(Request $request){
        $id_player = $request->input('id_player');
        $id_pokemon = $request->input('id_pokemon');

        $pc_obj = new PokemonCurrent();
        $pokemon = $pc_obj->getCurrentPokemon($id_pokemon);
        if($pokemon && !$pokemon->id_owner_player){
            $pokemon->id_catch_player = $id_player;
            $pokemon->id_owner_player = $id_player;
            $pokemon->save();

            $tc = new ThingCurrent();
            $pokeball_count = $tc->getPlayerThingCount($id_player, 3);
            $tc->setPlayerThingCount($id_player, 3, $pokeball_count-1);

            //добавить атаку
            $la = new LearnedAttaсk;
            $la->id_pokemon_current = $pokemon->id;
            $la->id_attack = 5;
            $la->save();

            //добавить в таблицу количество
            $al = new PokemonAttackLeft;
            $al->id_pokemon_current = $pokemon->id;
            $al->id_attack = 1;
            $al->left = 10;
            $al->save();

            return response()->json($pokemon, 200);

        }
        return response()->json([], 200);

    }

    public function update(Request $request, PokemonCurrent $current)
    {
        $current->update($request->all());

        return response()->json($current, 200);
    }

    public function delete(PokemonCurrent $current)
    {
        $current->delete();

        return response()->json(null, 204);
    }

    public function getAttacks($id_pokemon)
    {
        $attack_obj = new Attack();
        $attacks = $attack_obj->getAttackPokemon($id_pokemon);


        return response()->json($attacks, 200);

    }

    public function heal($id_player)
    {
        $data = [];
        $pc_obj = new PokemonCurrent();
        $treatment_cost = 0;
        $pokemons = $pc_obj->getPlayerPokemons($id_player);


        foreach ($pokemons as $pokemon) {


            $current_health = $pc_obj->getPokemonHealth($pokemon->id);

            if ($current_health && ($pokemon->health != $current_health)) {
                $difference = $pokemon->health - $current_health;

                $treatment_cost += $difference * $pokemon->level;
                $pc_obj->setPokemonCurrentHealth($pokemon->id, $pokemon->health);
            }

            $attack_obj = new Attack();
            $attacks = $attack_obj->getAttackPokemon($pokemon->id);

            foreach ($attacks as $attack) {

                $aca_obj = new AllowableNumberAttacks();
                $attack_ac = $aca_obj->getAllowableCountAttack($attack['id_attack']);

                if ($attack_ac != $attack['left']) {
                    $difference = $attack_ac - $attack['left'];
                    $treatment_cost += $difference * $pokemon->level;
                    $al_obj = new PokemonAttackLeft();
                    $al_obj->setAttackCount($pokemon->id, $attack['id_attack'], $attack_ac);
                }
            }
        }
        if($treatment_cost) {
            $tc_obj = new ThingCurrent();
            $credits = $tc_obj->getPlayerThingCount($id_player,1);
            $pc = $credits - $treatment_cost;
            $tc_obj->setPlayerThingCount($id_player, 1, $pc);

            $t_obj = new Thing();
            $data['img'] = $t_obj->getThingImg(1);
            $data['count'] = $treatment_cost;
            $data['status'] = false;
        }
        return response()->json($data, 200);
    }

    public function healAll()
    {

        $data = [];
        $pc_obj = new PokemonCurrent();
        $pokemons = $pc_obj->all();

        foreach ($pokemons as $pokemon) {


            $current_health = $pc_obj->getPokemonHealth($pokemon->id);

            if ($current_health && ($pokemon->health != $current_health)) {

                $pc_obj->setPokemonCurrentHealth($pokemon->id, $pokemon->health);
            }

            $attack_obj = new Attack();
            $attacks = $attack_obj->getAttackPokemon($pokemon->id);

            foreach ($attacks as $attack) {

                $aca_obj = new AllowableNumberAttacks();
                $attack_ac = $aca_obj->getAllowableCountAttack($attack['id_attack']);

                if ($attack_ac != $attack['left']) {

                    $al_obj = new PokemonAttackLeft();
                    $al_obj->setAttackCount($pokemon->id, $attack['id_attack'], $attack_ac);
                }
            }
        }
        $data['healAll'] = true;

        return response()->json($data, 200);
    }
}
