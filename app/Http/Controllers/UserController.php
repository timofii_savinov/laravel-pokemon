<?php

namespace App\Http\Controllers;

use App\PokemonCurrent;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getOpponent($id)
    {
        $data = [];
        if ($id) {
            $user = User::where('id', $id)->get()->toArray();
            $pc_obj = new PokemonCurrent();
            $pokemon = $pc_obj->getPlayerPokemons($id);
            $data['user'] = $user;
            $data['pokemon'] = $pokemon;
        }
        return response()->json($data, 200);
    }
}
