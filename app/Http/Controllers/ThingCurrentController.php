<?php

namespace App\Http\Controllers;

use App\ThingCurrent;
use Illuminate\Http\Request;

class ThingCurrentController extends Controller
{
    public function index()
    {
        return ThingCurrent::all();
    }

    public function show(ThingCurrent $thing)
    {
        return $thing;
    }

    public function create(Request $request)
    {
        $thing = ThingCurrent::create($request->all());

        return response()->json($thing, 201);
    }

    public function update(Request $request, ThingCurrent $thing)
    {
        $thing->update($request->all());

        return response()->json($thing, 200);
    }

    public function delete(ThingCurrent $thing)
    {
        $thing->delete();

        return response()->json(null, 204);
    }
}
