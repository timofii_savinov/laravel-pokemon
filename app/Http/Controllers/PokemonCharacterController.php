<?php

namespace App\Http\Controllers;

use App\PokemonCharacter;
use Illuminate\Http\Request;

class PokemonCharacterController extends Controller
{
    public function index()
    {
        return PokemonCharacter::all();
    }

    public function show(PokemonCharacter $character)
    {
        return $character;
    }

    public function create(Request $request)
    {
        $character = PokemonCharacter::create($request->all());

        return response()->json($character, 201);
    }

    public function update(Request $request, PokemonCharacter $character)
    {
        $character->update($request->all());

        return response()->json($character, 200);
    }

    public function delete(PokemonCharacter $character)
    {
        $character->delete();

        return response()->json(null, 204);
    }
}
