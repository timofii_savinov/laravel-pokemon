<?php

namespace App\Http\Controllers;

use App\Dialog;
use App\LastLocation;
use App\Location;
use App\Pokemon;
use App\Thing;
use App\User;
use App\PokemonCurrent;
use JavaScript;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /*game start screen*/
    public function index()
    {
        return view('game.first_game_screen');
    }

    public function location($location)
    {

        $user = auth()->user();

        //получаем покемонов игрока
        $pc_obg = new PokemonCurrent();
        $pokemons = $pc_obg->getPlayerPokemons($user->id);

        //проверка чтобы не получил еще одного покемона
        if ($user->id != 1) {
            if ($location == 'pokecenter' && !$pokemons->isEmpty()) {
                $location = 'region_kanto';
            }
        }


        //получаем данные для локации
        $location_cur = Location::where('link', $location)->firstOrFail();
        $data = $this->getDataFromLocationType($location_cur);
        $data['pokemons_currents'] = $pokemons;

        //передаем данные игрока на локацию
        $data['player']['img'] = $user->img;
        $data['player']['id'] = $user->id;

        //получаем предметы
        $data['things_currents'] = $this->getPlayerThings($user->id);

        //передаем данные игрока на локацию в js
        JavaScript::put([
            'player' => $data['player'],
        ]);

        //загружаем последнюю локацию
        $last_loc = LastLocation::where('id_player', $user->id)->first();
        if (!$last_loc) {
            $last_loc = new LastLocation;
            $last_loc->id_location = $location_cur->id;
            $last_loc->id_player = $user->id;
            $last_loc->save();
        } else {
            $last_loc->id_location = $location_cur->id;
            $last_loc->save();
        }
        $data['peoples'] = $this->getPeopleOnLocation($location_cur->id);


        return view('game.location')->with('data', $data);

    }

    public function getDataFromLocationType(Location $location)
    {

        switch ($location->type) {
            case Location::TYPE_LOCATION:
                $data = $this->getLocationScreenData($location);
                break;
            case Location::TYPE_DIALOG:
                $data = $this->getDialogScreenData($location);
                break;
            case Location::TYPE_FIGHT_AREA:
                $data = $this->getFightScreenData($location);
                break;
            case Location::TYPE_MAP:
                $data = $this->getMapScreenData($location);
                break;

        }
        return $data;
    }

    public function getLocationScreenData(Location $location)
    {

        $data['location'] = $location;
        return $data;
    }

    public function getDialogScreenData(Location $location)
    {
        $dialog = Dialog::where('id_location', '=', $location->id)->get()->toArray();

        foreach ($dialog as $replica_id => $replica) {
            $dialog['persons'][] = $replica['name'];
        }

        $dialog['persons'] = array_unique($dialog['persons']);
        $data['location'] = $location;
        $data['dialog'] = $dialog;

        JavaScript::put([
            'dialog' => $dialog,
        ]);

        //сделать на выбор 4-х покемонов
        if ($location['id'] == 1) {
            $pokemons = Pokemon::where('id','<=',6)->get();
            $data['pokemons'] = $pokemons;
        }

        return $data;
    }

    public function getMapScreenData(Location $location)
    {
        $data['locations'] = Location::whereIn("type", [Location::TYPE_FIGHT_AREA, Location::TYPE_LOCATION, Location::TYPE_DIALOG])->get();
        unset( $data['locations'][0]);
        $data['location'] = $location;
        return $data;
    }

    public function getFightScreenData(Location $location)
    {
        $data['location'] = $location;
        return $data;
    }

    public function getPeopleOnLocation($id)
    {
        $peoples = [];
        $on_location = LastLocation::where('id_location', $id)->get();
        $user = auth()->user();
        foreach ($on_location as $plp) {

            if ($user->id != $plp->id_player) {
                $user_c = User::where('id', $plp->id_player)->first()->toArray();
                $peoples[] = $user_c;
            }
        }
        return $peoples;
    }

    public function getPlayerThings($id_player)
    {
        $thing_obj = new Thing();
        $things = $thing_obj->getThingsPlayer($id_player);
        return $things;
    }
}
