<?php

namespace App\Http\Controllers;

use App\Thing;
use Illuminate\Http\Request;

class ThingController extends Controller
{
    public function index()
    {
        return Thing::all();
    }

    public function show(Thing $thing)
    {
        return $thing;
    }

    public function create(Request $request)
    {
        $thing = Thing::create($request->all());

        return response()->json($thing, 201);
    }

    public function update(Request $request, Thing $thing)
    {
        $thing->update($request->all());

        return response()->json($thing, 200);
    }

    public function delete(Thing $thing)
    {
        $thing->delete();

        return response()->json(null, 204);
    }
}
