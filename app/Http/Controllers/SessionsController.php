<?php

namespace App\Http\Controllers;

use App\LastLocation;
use App\Location;
use App\ThingCurrent;
use App\User;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function create()
    {
        return view('login.create');
    }

    public function store()
    {
        if (auth()->attempt(request(['email', 'password'])) == false) {
            return back()->withErrors([
                'message' => 'The email or password is incorrect, please try again'
            ]);
        }
        $user = auth()->user();

        //потом оптимизировать
        $last_loc = LastLocation::where('id_player', $user->id)->first();
        if ($last_loc) {
            $location = Location::where('id', $last_loc->id_location)->first();
            if ($location) {
                return redirect()->to('/game/' . $location->link);
            }
        }
        if ($user->getFt())
            return redirect()->to('/start');
        else
            return redirect()->to('/game/pokecenter');

    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $sex = $request->input('sex');
        $user->login = $request->input('login');
        $user->img = $request->input('player_img');

        $user->ft = 0;

        $user->save();

        $tc_obj = new ThingCurrent();

        $data[0]['id_owner'] = $user->id;
        $data[0]['count'] = 10;
        $data[0]['type_owner'] = 1;
        $data[0]['id_thing'] = 3;

        $data[1]['id_owner'] = $user->id;
        $data[1]['count'] = 500;
        $data[1]['type_owner'] = 1;
        $data[1]['id_thing'] = 1;
        foreach ($data as $value)
            $tc_obj->create($value);
        return redirect()->to('/game/pokecenter');
    }

    public function destroy()
    {
        auth()->logout();

        return redirect()->to('/');
    }
}
