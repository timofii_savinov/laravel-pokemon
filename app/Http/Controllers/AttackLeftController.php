<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttackLeftController extends Controller
{
    public function index()
    {
        return PokemonAttackLeft::all();
    }

    public function show(PokemonAttackLeft $attackLeft)
    {
        return $attackLeft;
    }

    public function create(Request $request)
    {
        $attackLeft = PokemonAttackLeft::create($request->all());

        return response()->json($attackLeft, 201);
    }

    public function update(Request $request, PokemonAttackLeft $attackLeft)
    {
        $attackLeft->update($request->all());

        return response()->json(attackLeft, 200);
    }

    public function delete(PokemonAttackLeft $attackLeft)
    {
        $attackLeft->delete();

        return response()->json(null, 204);
    }
}
