<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->get('/pokemons', 'GameControllers\PokemonAdminController@index')->name('admin.pokemon');


    $router->get('/pokemons/create', 'GameControllers\PokemonAdminController@create')->name('admin.pokemon.create');
    $router->get('/pokemons/show/{id}', 'GameControllers\PokemonAdminController@show')->name('admin.pokemon.show');
    $router->get('/pokemons/edit/{id}', 'GameControllers\PokemonAdminController@edit')->name('admin.pokemon.edit');
    $router->post('/pokemons/store', 'GameControllers\PokemonAdminController@store')->name('admin.pokemon.store');
    $router->post('/pokemons/update/{id}', 'GameControllers\PokemonAdminController@update')->name('admin.pokemon.update');
    $router->get('/pokemons/destroy/{id}', 'GameControllers\PokemonAdminController@destroy')->name('admin.pokemon.delete');


});
