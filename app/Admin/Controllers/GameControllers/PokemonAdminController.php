<?php

namespace App\Admin\Controllers\GameControllers;

use App\Pokemon;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PokemonAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Content $content)
    {
        $pokemons = Pokemon::paginate(15);

        return $content
            ->title('Pokemons')
            ->view('admin.pokemon.index', compact('pokemons'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Content $content)
    {
        return $content
            ->title('Pokemon create')
            ->view('admin.pokemon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'img' => 'required',
        ]);

        $file = $request->file('img');

        $name = $file->getClientOriginalName();
        $file->move('img/character/pokemons/', $name);

        $inputs = $request->all();
        $inputs['img'] = $name;
        Pokemon::create($inputs);


        return redirect()->route('admin.pokemon')
            ->with('success', 'Pokemon created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content, $id)
    {
        $pokemon = Pokemon::find($id);
        return $content
            ->title('Pokemon show')
            ->view('admin.pokemon.show', ['pokemon' => $pokemon]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content, $id)
    {
        $pokemon = Pokemon::find($id);
        return $content
            ->title('Pokemon edit')
            ->view('admin.pokemon.edit', ['pokemon' => $pokemon]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'img' => 'required',
        ]);

        $pokemon = Pokemon::find($id);
        $file = $request->file('img');

        $name = $file->getClientOriginalName();
        $file->move('img/character/pokemons/', $name);

        $inputs = $request->all();
        $inputs['img'] = $name;
        $pokemon->update($inputs);

        return redirect()->route('admin.pokemon')
            ->with('success', 'Pokemon updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->delete();

        return redirect()->route('admin.pokemon')
            ->with('success', 'Pokemon deleted successfully');
    }
}
