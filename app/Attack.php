<?php

namespace App;

use App\PokemonAttackLeft;
use Illuminate\Database\Eloquent\Model;

/**
 * Атаки покемонов
 *
 */
class Attack extends Model
{
    public $table = "attacks";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'accuracy', 'power'
    ];

    public function getAttackPokemon($id_pokemon)
    {

        $attacks = PokemonAttackLeft::where('pokemon_attack_left.id_pokemon_current', $id_pokemon)
            ->join('attacks', 'pokemon_attack_left.id_attack', '=', 'attacks.id')
            ->select( 'pokemon_attack_left.*','attacks.*')
            ->get()->toArray();

        return $attacks;
    }

    public function getCurrentAttack($id_attack, $id_pokemon){
        $attack = PokemonAttackLeft::where('pokemon_attack_left.id_pokemon_current', $id_pokemon)
            ->where('attacks.id', $id_attack)
            ->join('attacks', 'pokemon_attack_left.id_attack', '=', 'attacks.id')
            ->select( 'pokemon_attack_left.*','attacks.*')
            ->first();

        return $attack;
    }
}
