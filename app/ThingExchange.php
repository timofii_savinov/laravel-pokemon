<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Предметы на обмен между игроками
 *
 */
class ThingExchange extends Model
{
    public $table = "thing_exchanges";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_thing_current', 'count'
    ];
}
