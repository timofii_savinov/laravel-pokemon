<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllowableNumberAttacks extends Model
{
    public $table = "allowable_number_attacks";

    protected $fillable = [
        'id_attack', 'value'
    ];

    public function getAllowableCountAttack($id_attack)
    {
        $aca = $this->where('id_attack', $id_attack)->first();
        if($aca)
            return $aca->value;
        return false;
    }

}
