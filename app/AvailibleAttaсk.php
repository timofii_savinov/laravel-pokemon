<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Доступные атаки покемонов
 *
 */
class AvailibleAttaсk extends Model
{
    public $table = "available_attaсk";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_pokemon', 'id_attack'
    ];
}
