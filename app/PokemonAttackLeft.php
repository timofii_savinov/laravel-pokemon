<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonAttackLeft extends Model
{
    public $table = "pokemon_attack_left";

    public function updateCountLeft($id_attack, $id_pokemon)
    {
        $al = $this->where('id_attack', $id_attack)
            ->where('id_pokemon_current', $id_pokemon)
            ->first();

        if ($al && $al->left != 0) {
            if ($al->left) {
                $al->left = $al->left - 1;
                $al->save();
                return true;
            }
        }

        return false;
    }

    public function setAttackCount($id_pokemon, $id_attack, $value)
    {
        $al = $this->where('id_attack', $id_attack)
            ->where('id_pokemon_current', $id_pokemon)
            ->first();
        if($al){
            $al->left = $value;
            $al->save();
            return $al;
        }
        return false;
    }
}
