<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Типы покемонов
 *
 */
class PokemonType extends Model
{
    public $table = "pokemon_type";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
