<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Диалоги персонажей на локациях
 *
 */
class Dialog extends Model
{
    public $table = "dialog";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_location', 'name', 'text'
    ];
}
