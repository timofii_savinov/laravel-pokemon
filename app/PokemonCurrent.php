<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Существующие покумоны (пойманные) / если нет владельца покемон дикий
 *
 */
class PokemonCurrent extends Model
{
    public $table = "pokemon_currents";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pokemon_id', 'character_id', 'ability_id',
        'catch_player_id', 'genecode, level', 'happiness', 'health', 'attack',
        'defence', 'speed', 'special_attack', 'special_protection', 'evolution_points'
    ];

    public function getPlayerPokemons($id_player)
    {
        $pokemons = $this->where('id_owner_player',$id_player)->get();
        return $pokemons;
    }

    public function getCurrentPokemon($id)
    {
        $pokemon = $this->where('id',$id)->first();
        return $pokemon;
    }

    public function setPokemonCurrentHealth($id_pokemon, $health)
    {
        $cp = $this->where('id', $id_pokemon)->first();
        if ($cp) {
            $cp->current_health = $health;
            $cp->save();
        }

        return $cp->current_health;
    }

    public function setPokemonCurrentHealthWithDamage($id_pokemon, $damage)
    {
        $cp = $this->where('id', $id_pokemon)->first();
        if ($cp) {
            $h = $cp->current_health - $damage;
            if($h<=0)
                $h = 1;
            $cp->current_health =  $h;
            $cp->save();
        }

        return $cp->current_health;

    }

    public function getPokemonHealth($id_pokemon)
    {
        $cp = $this->where('id', $id_pokemon)->first();
        if ($cp)
            return $cp->current_health;
        return false;
    }

    public function getAll()
    {
        $pokemons = $this->all();
        return $pokemons;
    }
}
