<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Выученные атаки
 *
 */

class LearnedAttaсk extends Model
{
    public $table = "learned_attack";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_pokemon_current', 'id_attack'
    ];
}
