<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Все покемоны
 *
 */
class Pokemon extends Model
{


    public $table = "pokemon";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'img', 'health', 'attack', 'id_type_main', 'id_type_not_main',
        'defence', 'speed', 'special_attack', 'special_protection'
    ];

    public function getStatValue($id_pokemon, $base_stat, $stat_name, $id_char, $ev_points = 1, $lvl = 1)
    {
        //получаем генокод в цифрах
        $gen = PokemonGencode::where('id_pokemon', $id_pokemon)->first()->toArray();
        if ($gen) {

            if ($stat_name == 'health') {
                $value = ($base_stat * 2 + $gen[$stat_name] + $ev_points / 2) * $lvl / 100 + 10 + $lvl;
            } else {
                //получаем значение отношения характер/стат
                $char = RelationStatCharacter::where('id_character', $id_char)->first()->toArray();
                if ($char) {

                    $char_value = $char[$stat_name];

                    //добавить механики тренировок
                    $train = 1;
                    $value = (($base_stat * 2 + $gen[$stat_name] + $ev_points / 2) * $lvl / 100 + 5) * $char_value * $train;
                }
            }
            return round($value);
        }
        return false;
    }
}
