<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Таблица на обмен между персонажами
 *
 * @var array
 */
class RelationExchange_pp extends Model
{
    public $table = "relation_exchange_pps";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_player1', 'id_player2'
    ];
}
