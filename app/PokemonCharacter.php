<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Характер покемонов
 *
 */
class PokemonCharacter extends Model
{
    public $table = "pokemon_character";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
