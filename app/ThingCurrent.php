<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Созданные предметы
 *
 */
class ThingCurrent extends Model
{
    const TYPE_OWNER_POKEMON = 0;
    const TYPE_OWNER_PLAYER = 1;
    const TYPE_OWNER_CHARACTER = 2;

    public $table = "thing_currents";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_thing', 'id_owner', 'count', 'type_owner'
    ];

    //получить количество денег
    public function getPlayerThingCount($id_player, $id_thing)
    {
        $tc = $this->where('id_thing', $id_thing)
            ->where('id_owner', $id_player)
            ->first();
        if ($tc)
            return $tc->count;
        return false;
    }

    public function setPlayerThingCount($id_player, $id_thing, $count)
    {
        $tc = $this->where('id_thing', $id_thing)
            ->where('id_owner', $id_player)
            ->first();
        if ($tc) {

            $tc->count = $count;
            $tc->save();
            return true;
        }
        return false;
    }


    public function addCreditsAfterWin($id_pokemon_in_attack, $id_pokemon_attaсked)
    {
        $credits = 0;
        $pok_cur_obj = new PokemonCurrent();
        $pokemon_a = $pok_cur_obj->getCurrentPokemon($id_pokemon_attaсked);
        $pokemon_in_a = $pok_cur_obj->getCurrentPokemon($id_pokemon_in_attack);;
        $credits = round($pokemon_a->health*$pokemon_a->health*2/$pokemon_a->level);
        $tc = $this->where('id_thing', 1)
            ->where('id_owner', $pokemon_in_a->id_owner_player)
            ->first();
        if ($tc) {
            $tc->count = $tc->count + $credits;
            $tc->save();
        }
        return $credits;
    }
}
