<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Локации игры + локации диалогов
 *
 */
class Location extends Model
{
    const TYPE_LOCATION = 0;
    const TYPE_DIALOG = 1;
    const TYPE_FIGHT_AREA = 2;
    const TYPE_MAP = 3;
    const TYPE_EXCHANGE_AREA = 4;

    public $table = "location";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'closed', 'img', 'type', 'link'
    ];
}
