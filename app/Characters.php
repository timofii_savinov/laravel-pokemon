<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Игровые персонажи, персонажи диалогов
 *
 */
class Characters extends Model
{
    public $table = "characters";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'img'
    ];
}
