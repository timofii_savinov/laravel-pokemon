<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Модель ефективности типов
 *
 */
class TypeEffect extends Model
{
    public $table = "type_effects";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "type_main_id", "type_not_main_id", "type_opponent_id"
    ];

    public function getTypeEffectValue($pokemon_ina, $pokemon_a)
    {
        $type = TypeEffect::where('id_type_main', $pokemon_a->id_type_main)
            ->where('id_type_not_main', $pokemon_a->id_type_not_main)
            ->where('id_type_opponent', $pokemon_ina->id_type_main)->first();
        if(!$type)
            return 1;
        return $type->value;
    }
}
