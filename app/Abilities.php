<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель умений покемонов
 *
 */
class Abilities extends Model
{
    public $table = "abilities";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
