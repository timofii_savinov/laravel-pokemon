<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonCurrentHealth extends Model
{
    public $table = "current_pokemon_healths";

    protected $fillable = ['id_pokemon', 'current_health'];


}
