<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonGencode extends Model
{
    public $gen_name_array = ["H" => "health",
        "A" => "attack",
        "D" => "defence",
        "S" => "speed",
        "SA" => "special_attack",
        "SP" => "special_protection"];

    protected $fillable = [
        'id_pokemon', 'health', 'attack',
        'defence', 'speed', 'special_attack', 'special_protection'
    ];

    public function createGen($id)
    {
        $gen_code = new PokemonGencode();

        $data['id_pokemon'] = $id;
        $gen_code_str = '';

        foreach ($this->gen_name_array as $key => $value) {
            $val = rand(1, 32);
            $data[$value] = $val;
            $gen_code_str .= $value . $val;
        }

        $gen_code->fill($data);
        $gen_code->save();
        return $gen_code_str;
    }
}
