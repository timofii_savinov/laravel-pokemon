<?php

namespace App;

use App\ThingCurrent;
use Illuminate\Database\Eloquent\Model;

/**
 * Таблица предметов
 *
 * @var array
 */
class Thing extends Model
{
    public $table = "things";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'id_type_thing', 'img'
    ];

    public function getThingsPlayer($id)
    {
        $things = ThingCurrent::where('id_owner', $id)
            ->join('things', 'thing_currents.id_thing', '=', 'things.id')
            ->get()->toArray();
        return $things;
    }

    public function getThingImg($id)
    {
        $things = Thing::where('id', $id)->first();
        return $things->img;
    }
}
