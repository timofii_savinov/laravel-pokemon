<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/start', 'GameController@index')->name('start')->middleware('auth');
Route::get('/game/{name}', 'GameController@location')->name('game')->middleware('auth');

Route::get('/register', 'RegistrationController@create')->name('register');
Route::post('register', 'RegistrationController@store');

Route::get('login', 'SessionsController@create')->name('login');
Route::post('/login', 'SessionsController@store');
Route::post('update', 'SessionsController@update')->middleware('auth');
Route::get('/logout', 'SessionsController@destroy');

