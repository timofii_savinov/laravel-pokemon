<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

/*pokemon*/
Route::get('pokemon', 'PokemonController@index');
Route::get('pokemon/{id}', 'PokemonController@show');
Route::post('pokemon', 'PokemonController@create')->name('pokemon.create');
Route::put('pokemon/{id}', 'PokemonController@update');
Route::delete('pokemon/{id}', 'PokemonController@delete');

/*current pokemon*/
Route::get('pokemon_current', 'PokemonCurrentController@index');
Route::get('pokemon_current/{id}', 'PokemonCurrentController@show');
Route::post('pokemon_current', 'PokemonCurrentController@create');
Route::put('pokemon_current/{id}', 'PokemonCurrentController@update');
Route::delete('pokemon_current/{id}', 'PokemonCurrentController@delete');
Route::get('pokemon_current/getattacks/{id_pokemon}', 'PokemonCurrentController@getAttacks');
Route::post('pokemon_current/catch/', 'PokemonCurrentController@catch');
Route::get('pokemon_current/heal/{id_player}', 'PokemonCurrentController@heal');
Route::get('/heal/all', 'PokemonCurrentController@healAll');



/*pokemon type*/
Route::get('pokemon_type', 'PokemonTypeController@index');
Route::get('pokemon_type/{id}', 'PokemonTypeController@show');
Route::post('pokemon_type', 'PokemonTypeController@create');
Route::put('pokemon_type/{id}', 'PokemonTypeController@update');
Route::delete('pokemon_type/{id}', 'PokemonTypeController@delete');

/*pokemon abilities*/
Route::get('pokemon_abilities', 'PokemonAbilitiesController@index');
Route::get('pokemon_abilities/{id}', 'PokemonAbilitiesController@show');
Route::post('pokemon_abilities', 'PokemonAbilitiesController@create');
Route::put('pokemon_abilities/{id}', 'PokemonAbilitiesController@update');
Route::delete('pokemon_abilities/{id}', 'PokemonAbilitiesController@delete');

/*pokemon character*/
Route::get('pokemon_character', 'PokemonCharacterController@index');
Route::get('pokemon_character/{id}', 'PokemonCharacterController@show');
Route::post('pokemon_character', 'PokemonCharacterController@create');
Route::put('pokemon_character/{id}', 'PokemonCharacterController@update');
Route::delete('pokemon_character/{id}', 'PokemonCharacterController@delete');

/*location*/
Route::get('location', 'LocationController@index');
Route::get('location/{id}', 'LocationController@show');
Route::post('location', 'LocationController@create');
Route::put('location/{id}', 'LocationController@update');
Route::delete('location/{id}', 'LocationController@delete');

/*dialog*/
Route::get('dialog', 'DialogController@index');
Route::get('dialog/{id}', 'DialogController@show');
Route::post('dialog', 'DialogController@create');
Route::put('dialog/{id}', 'DialogController@update');
Route::delete('dialog/{id}', 'DialogController@delete');

/*attack*/
Route::get('attack', 'AttackController@index');
Route::get('attack/{id}', 'AttackController@show');
Route::post('attack', 'AttackController@create');
Route::put('attack/{id}', 'AttackController@update');
Route::delete('attack/{id}', 'AttackController@delete');

/*learned attack*/
Route::get('learned_attack', 'LearnedAttackController@index');
Route::get('learned_attack/{id}', 'LearnedAttackController@show');
Route::post('learned_attack', 'LearnedAttackController@create');
Route::put('learned_attack/{id}', 'LearnedAttackController@update');
Route::delete('learned_attack/{id}', 'LearnedAttackController@delete');

/*thing*/
Route::get('thing', 'ThingController@index');
Route::get('thing/{id}', 'ThingController@show');
Route::post('thing', 'ThingController@create');
Route::put('thing/{id}', 'ThingController@update');
Route::delete('thing/{id}', 'ThingController@delete');
/*thing_current*/
Route::get('thing_current', 'ThingCurrentController@index');
Route::get('thing_current/{id}', 'ThingCurrentController@show');
Route::post('thing_current', 'ThingCurrentController@create');
Route::put('thing_current/{id}', 'ThingCurrentController@update');
Route::delete('thing_current/{id}', 'ThingCurrentController@delete');

Route::get('thing_current/get_player_things/{id_player}', 'GameController@getPlayerThings');

Route::get('/opponent/{id}', 'UserController@getOpponent');
//обмен
/*
    * даем запрос на обмен
    */
Route::post('exchange/add', 'ExchangeController@addRequestExchange');

/*
 * даем согласие на обмен
 */
Route::get('exchange/confirm', 'ExchangeController@confirmRequestExchange');

/*
 * добавляем вещи на обмен
 */
Route::get('exchange/add-things', 'ExchangeController@addThingToExchange');

/*
 * даем добро на обмен 1й пользователь
 */
Route::get('exchange/confirm/1/{id}', 'ExchangeController@exchangeAcceptedPlayer1');

/*
 * даем добро на обмен 2й пользователь
 */
Route::get('exchange/confirm/2/{id}', 'ExchangeController@exchangeAcceptedPlayer2');



//бой c диким покемоном
/*
 * получаем покемона
 */
Route::get('fight/wild', 'PokemonCurrentController@createWildPokemon');
/*
 * удар выбранной атакой
 */
Route::post('fight/wild', 'FightController@stepWild');






