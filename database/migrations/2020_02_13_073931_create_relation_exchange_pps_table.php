<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationExchangePpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_exchange_pps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_player1');
            $table->integer('id_player2')->nullable();
            $table->boolean('id_player1_accepted')->default(0);
            $table->boolean('id_player2_accepted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_exchange_pps');
    }
}
