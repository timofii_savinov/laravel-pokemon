<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonGencodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_gencodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pokemon');
            $table->integer('health')->default(1);
            $table->integer('attack')->default(1);
            $table->integer('defence')->default(1);
            $table->integer('speed')->default(1);
            $table->integer('special_attack')->default(1);
            $table->integer('special_protection')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon_gencodes');
    }
}
