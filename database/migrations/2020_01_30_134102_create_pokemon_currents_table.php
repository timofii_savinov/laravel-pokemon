<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonCurrentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_currents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_pokemon");
            $table->integer("id_character");
            $table->integer("id_ability")->default(0);
            $table->integer("id_catch_player");
            $table->integer("id_owner_player");
            $table->string("genecode")->default('');
            $table->integer("level")->default(1);
            $table->integer("level_point")->default(0);
            $table->integer("happiness")->default(0);
            $table->integer("health")->default(0);
            $table->integer("current_health")->default(0);
            $table->integer("attack")->default(0);
            $table->integer("defence")->default(0);
            $table->integer("speed")->default(0);
            $table->integer("special_attack")->default(0);
            $table->integer("special_protection")->default(0);
            $table->integer("evolution_points")->default(1);
            $table->string("img")->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon_currents');
    }
}
