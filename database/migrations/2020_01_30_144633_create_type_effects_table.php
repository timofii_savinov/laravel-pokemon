<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeEffectsTable extends Migration
{
    /**
     * Run the migrations.
     * damage table types
     * 4 - Четвертной урон
     * 2 - Двойной урон
     * 0.5 - Половинчатый урон
     * 0.25 - Четвертовый урон
     * 0 - Нулевой урон
     * @return void
     */
    public function up()
    {
        Schema::create('type_effects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_type_main");
            $table->integer("id_type_not_main");
            $table->integer("id_type_opponent");
            $table->float('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_effects');
    }
}
