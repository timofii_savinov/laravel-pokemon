<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationStatCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_stat_characters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_character');
            $table->float('attack');
            $table->float('defence');
            $table->float('speed');
            $table->float('special_attack');
            $table->float('special_protection');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_stat_characters');
    }
}
