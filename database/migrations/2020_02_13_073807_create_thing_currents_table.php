<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThingCurrentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thing_currents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_thing');
            $table->integer('id_owner')->nullable();
            $table->integer('count')->default(1);
            $table->boolean('type_owner')->default(1); //0-pokemon/1-player
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thing_currents');
    }
}
