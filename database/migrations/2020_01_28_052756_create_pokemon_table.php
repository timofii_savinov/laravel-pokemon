<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonTable extends Migration
{

    /**
     * Run the migrations.
     *
     * бызовые статы покемонов - 'health', 'attack', 'defence', 'speed', 'special_attack', 'special_protection'
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_type_main');
            $table->integer('id_type_not_main')->default(0);
            $table->integer('health')->default(0);
            $table->integer('attack')->default(0);
            $table->integer('defence')->default(0);
            $table->integer('speed')->default(0);
            $table->integer('special_attack')->default(0);
            $table->integer('special_protection')->default(0);
            $table->string('name');
            $table->string('img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon');
    }
}
