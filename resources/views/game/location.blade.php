@extends('head')
@section('content')
    <div {{--style="background:url('{{url('/img/locations/'.$data['location']['img']) }}') no-repeat; background-size: cover;" --}}
         class="location">
        <h2>{{$data['location']['name']}}</h2>
       <!-- <button type="button" class="btn btn-success  heal-all-btn">HEAL ALL</button>-->
        <button type="button" class="btn btn-success  give-up">GIVE UP</button>
        <a href="/logout" type="button" class="btn btn-info logout-btn">LOGOUT</a>
        @if(isset($data['dialog']))
            @foreach($data['dialog']['persons'] as $person_id =>$person)
                @if($person!='player')
                    <div class="player-dialog {{$person}}" id="">

                    </div>
                @else
                    <div class="player-dialog player" data-player-id="{{$data['player']['id']}}"
                         style="background: url('{{$data['player']['img']}}') no-repeat;
                                 background-size: contain;">

                    </div>
                @endif
            @endforeach
        @endif
        @if(isset($data['pokemons']))
            <div class="pokemon-block">
                <div class="pokemon-choose">
                    @foreach($data['pokemons'] as $pokemon_id=> $pokemon)
                        <div class="pokemon" data-id="{{$pokemon->id}}">
                            <img src="{{url($pokemon->img) }}">
                            <span>{{$pokemon->name}}</span></div>
                    @endforeach
                </div>
            </div>
        @endif
        @if($data['location']['link']=='pokecenter_alabasia')

            <button type="button" class="btn btn-success  heal-btn">HEAL</button>

        @endif

        @if(isset($data['locations']))
            <ul>
                @foreach($data['locations'] as $location_id =>$location)
                    <li><a href="/game/{{$location->link}}">{{$location->name}}</a></li>
                @endforeach
            </ul>
        @endif
        <div id="sidebar-wrapper">
            <div class="sidebar-title">Trainers on location</div>
            <ul class="sidebar-nav player-on-location-list">
                @foreach($data['peoples'] as $people_num=> $people)
                    <li data-player-id="{{$people['id']}}">{{$people['name']}}</li>
                @endforeach
            </ul>
        </div>
        <div class="waiting-block">
            <div>Waiting for oponent</div>
            <div class="progress-wrap progress" data-progress-percent="100">
                <div class="progress-bar progress"></div>
            </div>
        </div>

    @if($data['location']->name!='pokecenter')
        <!-- Tab panes -->
            <div class="bottom-content">
                @if($data['location']->type != 1 && $data['location']->type != 3 && $data['location']->type != 4)
                    <div class="btn-group-toggle wild-pokemons" data-toggle="buttons">
                        <label class="btn btn-success">
                            <input type="checkbox" autocomplete="off"> wild pokemons
                        </label>
                    </div>
                @endif
                <div id="pokeball-tab" class="container show"><br>
                    @foreach($data['pokemons_currents'] as $pokemon_id=> $pokemon)
                        <div class="pokemon" data-id="{{$pokemon->id}}">
                            <img src="{{url($pokemon['img']) }}">
                            <span class="pokemon-health" style="width:{{($pokemon['current_health']/$pokemon['health'])*0.8*100}}%"></span>
                            <ul class="pokemon-info list-group">
                                <li class="list-group-item">level:{{$pokemon->level}}</li>
                                <li class="list-group-item">happiness:{{$pokemon->happiness}}</li>
                                <li class="list-group-item">health:{{$pokemon->health}}</li>
                                <li class="list-group-item">attack:{{$pokemon->attack}}</li>
                                <li class="list-group-item">defence:{{$pokemon->defence}}</li>
                                <li class="list-group-item">speed:{{$pokemon->speed}}</li>
                                <li class="list-group-item">special_attack:{{$pokemon->special_attack}}</li>
                                <li class="list-group-item">special_protection:{{$pokemon->special_protection}}</li>
                                <li class="list-group-item">evolution_points:{{$pokemon->evolution_points}}</li>
                            </ul>
                        </div>
                    @endforeach

                </div>
                <div id="bag-tab" class="container hide"><br>
                    @foreach($data['things_currents'] as $thing_id => $thing)
                        <div class="thing" data-id="{{$thing['id']}}">
                            <img src="{{url('img/'.$thing['img'])}}">
                            <span class="count">{{$thing['count']}}</span>
                        </div>
                    @endforeach
                </div>
                <div id="chat-tab" class="container hide"><br>
                    Тут будет чат
                </div>
                <div class="menu">
                    <div id="pokeball"><img src="{{url('img/pokeball.png')}}"></div>
                    <div id="bag"><img src="{{url('img/bag.png')}}"></div>
                    <div id="map"><a href="/game/region_kanto"><img src="{{url('img/map.png')}}"></a></div>
                    <div id="chat"><img src="{{url('img/chat.png')}}"></div>
                </div>
            </div>


    @endif
@endsection





