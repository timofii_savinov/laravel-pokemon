@extends('head')
@section('content')
    <div style="background:url('{{url('/img/locations/'.$data['location']['img']) }}') no-repeat; background-size: cover;"
         class="location">
        @foreach($data['dialog']['persons'] as $person_id =>$person)
            @if($person!='player')
                <div class="player-dialog {{$person}}">
                    <div class="message-orange">
                        <p class="message-content">I agree that your message is awesome!</p>
                    </div>
                </div>
            @else
                <div class="player-dialog player" style="background: url('{{$data['player']['img']}}')no-repeat;
                        background-size: contain;">
                    <div class="message-orange">
                        <p class="message-content">I agree that your message is awesome!</p>
                    </div>
                </div>
            @endif
        @endforeach
        @if(isset($data['pokemons']))
            <div class="pokemon-block">
                <div class="pokemon-choose">
                    @foreach($data['pokemons'] as $pokemon_id=> $pokemon)
                        <div style="background:url('{{url('img/character/pokemons/'.$pokemon['img']) }}') no-repeat; background-size: contain;" class="pokemon"></div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>

@endsection

