@extends('head')
@section('content')

    <div class="justify-content-center text-center ">
        <h1> Choose youre trainer </h1>
        <form method="POST" action="/update">
            @csrf
                <div class="input-group  w-50 mb-2 mx-auto">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">Trainer name:</span>
                    </div>
                    <input type="text" class="form-control" name="login"  placeholder="name"
                           aria-describedby="basic-addon2">


            </div>
            <div class="sex-choose">
                <div class="sex">
                    <input type="radio" name="sex" id="men" value=0 class="input-hidden men" required/>
                    <label for="men">
                        <img src="{{ url('/img/sex/mars.png') }}" alt="men"/>
                    </label>

                    <input type="radio" name="sex" id="women" value=1 class="input-hidden women" required/>
                    <label for="women">
                        <img src="{{ url('/img/sex/venus.png') }}" alt="women"/>
                    </label>
                </div>
                <div class="persons position-relative">
                    <div class="boys ">
                        <input type="radio" name="player_img" id="boys1" value="/img/character/men/1.png" class="input-hidden men" />
                        <label for="boys1">
                            <img src="{{ url('/img/character/men/1.png') }}" alt="men"/>
                        </label>

                        <input type="radio" name="player_img" id="boys2" value="/img/character/men/2.png" class="input-hidden women"/>
                        <label for="boys2">
                            <img src="{{ url('/img/character/men/2.png') }}" alt="men"/>
                        </label>
                    </div>
                    <div class="girls">
                        <input type="radio" name="player_img" id="girls1" value="/img/character/women/1.png" class="input-hidden"/>
                        <label for="girls1">
                            <img src="{{ url('/img/character/women/1.png') }}" alt="women"/>
                        </label>

                        <input type="radio" name="player_img" id="girls2" value="/img/character/women/2.png" class="input-hidden"/>
                        <label for="girls2">
                            <img src="{{ url('/img/character/women/2.png') }}" alt="women"/>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group choose-button">
                <button style="cursor:pointer" type="submit" class="btn btn-primary">ВЫБРАТЬ</button>
            </div>
        </form>
    </div>
@endsection
