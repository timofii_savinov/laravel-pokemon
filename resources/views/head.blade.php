<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->

        <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css')}}">
        <script src="{{ url('/js/jquery-3.1.0.js')}}" ></script>
        <script src="{{ url('/js/popper.min.js')}}" ></script>
        <script src="{{ url('/js/bootstrap.min.js')}}" ></script>
        <link rel="stylesheet" type="text/css" href="{{ url('/css/app.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/alert.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/bag.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/characters.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/dialog.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/fight-area.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/menu.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/sidebar.css') }}" />
        <script type="text/javascript" src="{{ url('/js/app.js') }}" ></script>
        <!-- Styles -->

    </head>
    <body>
    <div class="container">
    @yield('content')
    </div>
    <script src="{{url('/js/sidebar.js')}}"></script>
    <script src="{{url('/js/fight.js')}}"></script>
    <script src="{{url('/js/dialog.js')}}"></script>
    <script src="{{url('/js/game.js')}}"></script>
    </body>
</html>

