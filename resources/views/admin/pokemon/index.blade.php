@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('admin.pokemon.create') }}"> Create New Pokemon</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th scope="col" width="280px">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($pokemons as $pokemon)
            <tr>
                <td>{{ $pokemon->id}}</td>
                <td>{{ $pokemon->name }}</td>
                <td><img src="{{url('/img/character/pokemons/'.$pokemon->img) }}"></td>
                <td>
                    <form action="" method="POST">

                        <a class="btn btn-info" href="{{ route('admin.pokemon.show',['id'=>$pokemon->id]) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('admin.pokemon.edit',['id'=>$pokemon->id]) }}">Edit</a>

                        <a class="btn btn-danger" href="{{ route('admin.pokemon.delete',['id'=>$pokemon->id]) }}">Delete</a>


                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $pokemons->links() !!}

    <script>
        $(document).ready(function(){
             $('.alert-success').animate({
                 opacity : 0
             }, 1000, function() {
                 $(this).css('display','none');
             });
        });
    </script>
@endsection
